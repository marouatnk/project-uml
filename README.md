# Contexte
Plusieurs centres de formation de la région de l´Adour disposent de plusieurs formations ainsi que de nombreux apprenants et formateurs, un logiciel doit être concu afin de faciliter la gestion de ces centres de formations.

Les besoins exprimés par les centres de formation sont les suivants :

* Les directeurs de centre de la région doivent pouvoir gérer les différents centres de formation.
* Les chargés de promotions doivent pouvoir gérer les promotions du ou des centres auxquels sont rattachés
    * Créer des nouvelles promotions.
    * Inviter des apprenants dans les promotions.
    * Valider une candidature.
    * Assigner un/une formateur/trice à une promotion.
    * Gérer les retards et absences des apprenants.
* Les formateurs doivent pouvoir créer des projets puis les assigner à leurs promos ainsi que les corriger.
* On doit être capable de consulter l'agenda des formations en cours ou à venir.
* On doit pouvoir candidater à une formation à venir.
* Un apprenant doit avoir accès aux projets proposés à sa promotion ainsi que proposer un rendu.

Les apprenants doit également pouvoir consulter leur avancement dans les compétences.

Les promotions sont liées à un référentiel qui détermine les compétences et les projets assignables à cette promotion. Il est probable que d'autres référentiels soient rajoutés par la suite.

# Diagrammes de cas d´utilisation :

#### Ensemble du projet :

![global](./conception/cas-d-utilisation/global.png)

#### Authentifiaction :

![authentification](./conception/cas-d-utilisation/authentification.png)

#### Promotion :

![promotion](./conception/cas-d-utilisation/promotion.png)

#### Agenda :

![agenda](./conception/cas-d-utilisation/agenda.png)

#### Projet :

![projet](./conception/cas-d-utilisation/projet.png)

#### Référentiel :

![referentiel](./conception/cas-d-utilisation/referentiel.png)


# Diagrammes d'activités :

#### Pour le cas d'utilisation ' Créer un projet ' : 

![creer-projet](./conception/diagramme-d-activité/creer-un-projet.png)

#### Pour le cas d'utilisation ' Ajouter des apprenants à une promotion ' : 

![ajouter-apprenant-a-une-promotion](./conception/diagramme-d-activité/ajouter-un-apprenant-a-une-promotion.png)

#### Pour le cas d'utilisation ' Créer un compte' : 

![creer-un-compte](./conception/diagramme-d-activité/creer-un-compte.png)

#### Pour le cas d'utilisation 'Soumettre une candidature ' : 

![soumettre-une-candidature](./conception/diagramme-d-activité/soumettre-une-candidature.png)


# Scénarios 

#### Authentification : 

|      Nom                 | Création de compte                  |
| ------------------------ | --------------------------------- |
| Référence (ID)           |  #1                                 |
| Acteur(s)  Secondaire(s) | Utilisateurs                      |
| Date création            | 17/08/2020                        |
| Responsable              | Haris                             |
| Pré-condition            | Accessible pour tout le monde     |
| Scénario Nominal         | 1. Créer un compte <br> 2. Valider le compte via e-mail <br> 3. Afficher les informations <br> 4.Choisir formation <br> 5. Soumettre candidature <br> 6. Attendre validation |
| Post-condition           | Un nouvel utilisateur a été créé. |
| Scénarios alternatifs    | A1. <br> à partir de 1. <br> 2. Données non valide <br> 1.Affichage d’une erreur <br> 2.Recommencer  <br> B1.<br> à partir de 2 <br> 3. aucun e-mail reçu <br> 1. Compte non validé <br> 2. Mettre un e-mail valide <br> C3 <br> à partir de 1 <br> 3. Se connecter <br> 1. Accéder à la plateforme <br> 2. utiliser les différentes fonctionnalités |

#### Projet :

|      Nom                 | Assigner un projet à un apprenant |
| ------------------------ | --------------------------------- |
| Référence (ID)           | #2             |
| Acteur(s)  Secondaire(s) | Formateur                         |
| Date création            | 17/08/2020                        |
| Responsable              | Maroua Tounekti                   |
| Pré-condition            | S’authentifier / Créer un projet  |
| Scénario Nominal         | 1. Sélectionner une promotion <br> 2. Afficher la liste des apprenants <br> 3. Choisir le.s projet.s à assigner <br> 4. Valider L'assignation |
| Post-condition           | Projet assigné.                   |
| Scénarios alternatifs    | A1. <br> à partir de 1. <br> 2. Abandonner l’assignation <br> E1. <br> à partir de 4 <br> 5. Aucune promotion sélectionnée <br> 6.Affichage d’une erreur <br> 7.Sélectionner une promotion |


#### Affichage référentiel :

|      Nom                 | Affichage référentiel                       |
| ------------------------ | --------------------------------- |
| Référence (ID)           | #3                      |
| Acteur(s)  Secondaire(s) | Directeur <br> Formateur <br> Chargé de promotions  |
| Date création            | 17/08/2020                        |
| Responsable              | Anaelle                           |
| Pré-condition            | S’authentifier                    |
| Scénario Nominal         | 1. S’authentifier <br> 2. Accéder à tous les référentiels <br> 3. Accéder au référentiel d'une promotion spécifique <br> 4. Accéder aux projets assignables et assignés d'une promotion spécifique <br> 5. Accéder aux compétences requises d'une promotion spécifique |
| Post-condition           | Erreur d'authentification         |
| Scénarios alternatifs    | A1. <br> à partir de 1. <br> 2. Echech d'authentification <br> 3. Affichage du message d'erreur |

#### Déclarer un apprenant absent :

|      Nom                 | Déclarer un apprenant absent                       |
| ------------------------ | --------------------------------- |
| Référence (ID)           | #4                      |
| Acteur(s)  Secondaire(s) | Directeur <br> Formateur <br> Chargé de promotions  |
| Date création            | 18/08/2020                        |
| Responsable              | Florian                           |
| Pré-condition            | S’authentifier                    |
| Scénario Nominal         | 1. Afficher tous les apprenants. <br> 2. Séléctionner un apprenant. <br> 3. Le déclarer absent. |      |
| Scénarios alternatifs    | A1. <br> 1. Afficher la liste des promotions <br> 2. Sélectionner une promotion. <br> 3. Sélectionner un apprenant. <br> 4. Le déclarer absent. | 

# Diagrammes des classes :

#### Projet :

![projet](./conception/diagrammes-de-classes/projet.png)

#### Référenciel et Compétence : 

![competences-et-referentiel](./conception/diagrammes-de-classes/competences-et-referentiel.png)

#### Utilisateur : 

![utilisateur](./conception/diagrammes-de-classes/utilisateur.png)

#### Promotion : 

![promotion](./conception/diagrammes-de-classes/promotion.png)

#### Agenda : 

![agenda](./conception/diagrammes-de-classes/agenda.png)

#### Candidature : 

![candidature](./conception/diagrammes-de-classes/candidature.png)

# Diagrammes de séquences :

#### Ajouter une compétence à un référenciel : 

![ajouter-une-competence-au-referentiel](./conception/diagram-de-sequence/ajouter-une-competence-au-referentiel.png)

![afficher-les-prochaines-formations](./conception/diagram-de-sequence/afficher-les-prochaines-formations.png)

#### Créer un compte : 

![ajouter-une-competence-au-referentiel](./conception/diagram-de-sequence/creer-un-compte.png)

#### Afficher les prochaines formations : 

![ajouter-une-competence-au-referentiel](./conception/diagram-de-sequence/afficher-les-prochaines-formations.png)

# Diagramme d´état de transition :

#### Envoyer une candidature: 

![envoyer-une-candidature](./conception/diagramme-d-etat-de-transition/envoyer-une-candidature.png)

# Diagrammes de déploiement :

![global](./conception/diagramme-de-deploiment/global.png)

# Maquettes fonctionelles :

#### Envoyer une candidature: 

![envoyer-une-candidature-mobile](./conception/maquettes-fonctionnelles/envoyer-candidature-mobile.png)

![envoyer-une-candidature-ordinateur](./conception/maquettes-fonctionnelles/envoyer-candidature-ordinateur.png)

#### Afficher les promotions: 

![afficher-promotions-mobile](./conception/maquettes-fonctionnelles/afficher-promotions-mobile.png)

![afficher-promotions-ordinateur](./conception/maquettes-fonctionnelles/afficher-promotions-ordinateur.png)

#### Soumettre un rendu : 

![soumettre-un-rendu-mobile](./conception/maquettes-fonctionnelles/soumettre-un-rendu-mobile.png)

![soumettre-un-rendu-ordinateur](./conception/maquettes-fonctionnelles/soumettre-un-rendu-ordinateur.png)
